# Litecoin K8s

A project for deploying a Litecoin Docker image to Kubernetes using Jenkins.

## Building

You can build the container by running:

```
cd docker-image
docker build -t rwmnz/litecoin:0.18.1 .
```

Then push it to Docker hub:

```
docker push rwmnz/litecoin:0.18.1
```

## Deploying

Deployment to Kubernetes is handled by the Jenkins pipeline.

You can render each of the environments YAML manifests to see what they will look like:

```
kustomize build kustomize/test
kustomize build kustomize/prod
```

The test environment will be deployed against the Litecoin testnet and the prod environment
will be deployed against the real network.

## Terraform IAM Setup

You can execute the Terraform IAM setup for an environment by running the following:

```
cd aws-roles/
terraform init
terraform apply
```

## Scripts

After running the Docker container you can check which IP addresses are connecting to your
node.

Using the shell script:

```
./peers.sh ecstatic_dubinsky
```

Using the golang program:

```
go build .
./peers ecstatic_dubinsky
```

The output will look like:

```
37.59.32.10
167.179.147.155
34.239.163.148
51.195.5.189
77.247.178.158
3.68.34.196
95.179.138.134
89.47.166.94
```