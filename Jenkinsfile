// This Jenkins pipeline will build the Litecoin Docker image within the repository and perform a
// container scan using grype. If the master branch is being built it will tag the image with a
// unique version number and push it to DockerHub. It will then deploy the container image to
// the test namespace. There will be a prompt to deploy the build into the production namespace.

// This variable is populated in the later stages of the pipeline
def imageVersion;

pipeline {
    agent any

    options {
        ansiColor('xterm')
    }

    environment {
        DOCKER_REPO = "rwmnz/litecoin"
        DOCKER_HUB_CREDENTIALS = "docker-hub-rwmnz"
        KUBERNETES_CLUSTER_CREDENTIALS = "mercury-kubeconfig"
    }

    stages {
        stage("Build Image") {
            steps {
                dir("docker-image") {
                    sh "docker build --no-cache -t ${DOCKER_REPO}:${GIT_COMMIT} ."
                }
            }
        }
        stage("Vulnerability Scan") {
            steps {
                sh "docker run --rm -v /var/run/docker.sock:/var/run/docker.sock anchore/grype ${DOCKER_REPO}:${GIT_COMMIT} -f high"
            }
        }
        stage("Tag & Push") {
            when {
                branch "master"
            }
            steps {
                script {
                    // Determine a unique version for this image based on the version of Litecoin and the build number
                    // of this job.
                    litecoinVersion = sh (
                        returnStdout: true,
                        script: 'grep \'^ENV LITECOIN_VERSION\' docker-image/Dockerfile | awk -F= \'{print $2}\''
                    ).trim()
                    imageVersion = "${litecoinVersion}.${BUILD_NUMBER}"
                }
                withDockerRegistry([credentialsId: DOCKER_HUB_CREDENTIALS, url: ""]) {
                    sh """
                    docker tag ${DOCKER_REPO}:${GIT_COMMIT} ${DOCKER_REPO}:${imageVersion}
                    docker push ${DOCKER_REPO}:${imageVersion}
                    """
                }
            }
        }
        stage("Deploy Test") {
            when {
                branch "master"
            }
            agent {
                docker {
                    image "line/kubectl-kustomize:1.23.0-4.4.1"
                    args "--entrypoint=''"
                }
            }
            steps {
                withKubeConfig(credentialsId: KUBERNETES_CLUSTER_CREDENTIALS) {
                    dir("kustomize/base") {
                        sh "kustomize edit set image rwmnz/litecoin=:${imageVersion}"
                    }
                    dir("kustomize") {
                        sh "kustomize build test | kubectl apply -f -"
                    }
                }
            }
        }
        stage("Deploy Prod") {
            input {
                message "Deploy to Production?"
            }
            when {
                beforeInput true
                branch "master"
            }
            agent {
                docker {
                    image "line/kubectl-kustomize:1.23.0-4.4.1"
                    args "--entrypoint=''"
                }
            }
            steps {
                withKubeConfig(credentialsId: KUBERNETES_CLUSTER_CREDENTIALS) {
                    dir("kustomize/base") {
                        sh "kustomize edit set image rwmnz/litecoin=:${imageVersion}"
                    }
                    dir("kustomize") {
                        sh "kustomize build prod | kubectl apply -f -"
                    }
                }
            }
        }
    }
}