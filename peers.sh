#!/bin/bash

# Given a Docker container name or ID this script will list
# the peer IP addresses that are currently connected to your
# Litecoin Docker node.
#
# Usage: ./peers.sh <container ID/name>

if [ -z "$1" ]
  then
    echo "Must specify Docker container ID or name"
fi

echo "AWK"
echo "----------------"
sudo nsenter -t "$(docker inspect -f '{{.State.Pid}}' "$1")" -n ss | \
  awk '{ if ($2 == "ESTAB") { split($6, a, ":"); if (a[2] == "9333") print a[1] } }'
echo ""

echo "sed/grep"
echo "----------------"
sudo nsenter -t "$(docker inspect -f '{{.State.Pid}}' "$1")" -n ss | \
 grep 'ESTAB' | grep -oP "\d+\.\d+\.\d+\.\d+:9333" | sed 's/:9333$//g'