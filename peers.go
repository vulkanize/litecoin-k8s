package main

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"regexp"
	"strings"
)

// A utility to check the peers which are connected to your Litecoin node
//
// Usage: ./peers <container ID or name>
//
func main() {
	if len(os.Args) != 2 {
		fmt.Println("You must specify the container ID or name")
		os.Exit(1)
	}

	containerId := os.Args[1]

	socketStats, err := getSocketStats(containerId)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	parseSocketStats(socketStats)
}

// Parses the Litecoin peer IP addresses from the socket stats output
func parseSocketStats(stats string) {
	scanner := bufio.NewScanner(strings.NewReader(stats))
	for scanner.Scan() {
		line := scanner.Text()
		regex := regexp.MustCompile(`.*ESTAB.* (\d+\.\d+\.\d+\.\d+):9333`)
		result := regex.FindStringSubmatch(line)
		if result != nil {
			fmt.Println(result[1])
		}
	}
}

// Retrieves the socket stats for the provided docker container ID or name
func getSocketStats(containerId string) (ss string, err error) {
	pid, err := runCommand("docker", "inspect", "-f", "{{.State.Pid}}", containerId)
	if err != nil {
		return
	}
	ss, err = runCommand("sudo", "nsenter", "-t", pid, "-n", "ss")
	if err != nil {
		return
	}
	return
}

// Executes an operating system command and returns the output or returns
// an error if the command failed to execute.
func runCommand(name string, args ...string) (output string, err error) {
	cmd := exec.Command(name, args...)
	stdout, err := cmd.Output()
	if err != nil {
		return
	}
	output = strings.TrimSpace(string(stdout))
	return
}
