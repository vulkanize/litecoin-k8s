# Sets up the CI permission structure for the given environment.

variable "environment" {
  type = string
  default = "development"
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = "us-east-2"
}

# Get the current account ID
data "aws_caller_identity" "current" {}

# A role with no permissions with a trust relationship setup for the same account.
data "aws_iam_policy_document" "ci-iam-assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
    }
  }
}
resource "aws_iam_role" "ci-role" {
  name = "${var.environment}-ci-role"
  assume_role_policy = data.aws_iam_policy_document.ci-iam-assume-role-policy.json
}

# A policy allowing users or entities to assume the above role
data "aws_iam_policy_document" "ci-iam-assume-policy" {
  statement {
    actions = ["sts:AssumeRole"]
    resources = [aws_iam_role.ci-role.arn]
  }
}
resource "aws_iam_policy" "ci-policy" {
  name = "${var.environment}-ci-policy"
  policy = data.aws_iam_policy_document.ci-iam-assume-policy.json
}

# A group with the above policy attached
resource "aws_iam_group" "ci-group" {
  name = "${var.environment}-ci-group"
}
resource "aws_iam_group_policy_attachment" "ci-group-attachment" {
  group      = aws_iam_group.ci-group.name
  policy_arn = aws_iam_policy.ci-policy.arn
}

# A user belonging to the above group
resource "aws_iam_user" "ci-user" {
  name = "${var.environment}-ci-user"
}
resource "aws_iam_user_group_membership" "ci-user-group-membership" {
  user = aws_iam_user.ci-user.name
  groups = [
    aws_iam_group.ci-group.name,
  ]
}